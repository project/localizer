<?php
include_once(drupal_get_path('module', 'localizer') . '/models/localizernode.php');
include_once(drupal_get_path('module', 'localizer') . '/includes/localizer.inc');

/**
* Localizer node module.
* @author Roberto Gerola, 2006, http://www.speedtech.it
*/

/**
* Implementation of the hook menu for adding localizernode menu items.
*/
function localizernode_menu($may_cache) {
  $items = array();

  if(!$may_cache) {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $node = node_load(arg(1));
      $content_types_enabled = variable_get('localizer_contents_types', array('page'=>'page', 'story'=>'story'));
      if(array_key_exists($node->type, $content_types_enabled) && $content_types_enabled[$node->type]) {
        $items[] = array(
          'path' => 'node/'. arg(1) .'/translations',
          'title' => t('Translations'),
          'callback' => 'drupal_get_form',
          'callback arguments' => 'localizernode_translations',
          'access' => user_access('Localizer content: access translations'),
          'type' => MENU_LOCAL_TASK,
          'weight' => 3
        );
      }
    }
    $items[] = array(
      'path' => 'admin/content/localizer/content',
      'title' => t('Localizer content translation'),
      'description' => t('Manage Localizer content translations'),
      'callback' => 'localizernode_content',
      'access' => user_access('Localizer content: administer translations'),
      'weight' => 19,
      'type' => MENU_NORMAL_ITEM
    );
    $items[] = array(
      'path' => 'admin/content/localizer/content/translation/remove',
      'title' => t('Remove node from translation set'),
      'callback' => 'localizernode_translation_remove',
      'access' => user_access('Localizer content: disconnect translations'),
      'type' => MENU_CALLBACK,
    );
    $items[] = array(
      'path' => 'admin/content/localizer/content/translation/create',
      'title' => t('Create a new translation'),
      'callback' => 'localizernode_translation_create',
      'access' => user_access('Localizer content: create translations'),
      'type' => MENU_CALLBACK,
    );
    $items[] = array(
      'path' => 'localizernode/book_parents_js',
      'title' => t('Parents for a book (js)'),
      'callback' => '_localizernode_book_parents_js',
      'access' => user_access('access content'),
      'type' => MENU_CALLBACK
    );
  }
  return $items;
}

function localizernode_nodeapi(&$node, $op, $teaser, $page) {
  global $user;

  $contents_types_enabled = variable_get('localizer_contents_types', array('page'=>'page', 'story'=>'story'));

  switch ($op) {
    case 'load':
      if($contents_types_enabled[$node->type]) {
        $localizernode = localizernode_find_by_nid($node->nid);
        return array('language' => $localizernode['language'], 'pid' => $localizernode['pid']);
      }
      break;
    case 'insert':
    case 'update':
      if($contents_types_enabled[$node->type]) {
        $localizernode = array('nid'=>$node->nid, 'pid'=>$node->pid, 'language'=>$node->language);
        if($node->language) {
          localizernode_save($localizernode);
        }
      }
      break; 
    case 'delete':
      if($contents_types_enabled[$node->type]) {
        localizernode_delete($node->nid);
      }
      break;
    case 'validate':
      if($user->uid) {
        $languages = localizer_supported_languages();
        if(!_localizer_access_translation($languages[$node->language])) {
          form_set_error('language', t('You are not granted the permission to use ' . $languages[$node->language] . ' language.'));
        }
        if($node->pid) {
          $tr = localizernode_findone("pid=" . $node->pid . " AND language='" . $node->language . "'" );
          if(array_key_exists('nid', $tr) && $tr['nid']!=$node->nid) {
            $trnode = node_load($tr['nid']);
            $m = t('A translation for %language language is already present in this set.', array('%language' => $languages[$node->language])) . '<br />';
            $m .= t('Remove the translation ') . l($trnode->title, 'node/' . $trnode->nid) . ' from this set before proceeding';
            form_set_error('language', $m);
          }
        }
      }
      break;
  }
}

function localizernode_form_alter($form_id, &$form) { 

if (!isset($form['type'])) { 
  return;
}

switch ($form_id) { 
  case $form['type']['#value'] .'_node_form':
    $contents_types_enabled = variable_get('localizer_contents_types', array('page'=>'page', 'story'=>'story'));
    if($contents_types_enabled[$form['type']['#value']]) {
      $languages = localizer_supported_languages();
      if (arg(1)=='add' || arg(2)=='add') {
        if (variable_get('localizer_switch_byhostname', FALSE)) {
          $currenthost = $_SERVER['HTTP_HOST'];
          $languagebyhostname = localizer_language_by_hostname($currenthost);
          $form['#node']->language = $languagebyhostname;
        }
        else {
          $form['#node']->language = localizer_get_language();
        }
        $tlanguages = array();
        foreach($languages as $l=>$n) {
          if(_localizer_access_translation($n)) {
            $tlanguages[$l] = $n;
          }
        }
        $languages = $tlanguages; 
      }

      $is_book = FALSE;
      if(arg(0)=='node' && arg(1)=='add' && arg(2)=='book') {
        $is_book = TRUE;
        $nid_exclude = 0;
      }
      else if(arg(0)=='node' && arg(1) && is_numeric(arg(1))) {
        $node = node_load(arg(1));
        if($node->type=='book') {
          $is_book = true;
          $nid_exclude = arg(1);
        }
      }

      if($is_book) {
        $settings = array('localizernode' => array (
                      'book_parents_url' => url('localizernode/book_parents_js/')
                     )
                );
        drupal_add_js($settings, 'setting');
        drupal_add_js(drupal_get_path('module', 'localizer') . '/js/localizer.js', 'module', 'header', FALSE);  
        $attributes = array('onchange'=>"localizernode_book_parents(this, 'edit-parent', " . $nid_exclude . ")");
      }
      else {
        $attributes = array();
      }

      $form['language'] = array(
        '#type' => 'select',
        '#title' => t('Language'),
        '#default_value' => $form['#node']->language,
        '#options' => $languages,
        '#required' => FALSE,
        '#weight' => -18,
        '#attributes' => $attributes,
      );
      $form['pid'] = array(
        '#type' => 'hidden',
        '#default_value' => $form['#node']->pid,
      );
    }
    break;
   }
}

function localizernode_translations() {
  global $user;
  $languages = localizer_supported_languages();
  $node = node_load(arg(1));
  drupal_set_title($node->title);
  $form = array();

  $form['nid'] = array('#type'=>'hidden', '#value'=>$node->nid);
  $form['pid'] = array('#type'=>'hidden', '#value'=>$node->pid);
  $form['language'] = array('#type'=>'hidden', '#value'=>$node->language);
  $form['node_language'] = array (
                            '#type' => 'item',
                            '#title' => t('Content language'),
                            '#value' => $languages[$node->language],
                           );

  foreach($languages as $l=>$n) {
    $trnid = localizernode_get_localized_nid($node->nid, $l);
    if($trnid != $node->nid) {
      $trnode = node_load($trnid);
      $form['languages']['nodefor|' . $l] = array(
        '#type' => 'item',
        '#title' => t($n),
        '#value' => l($trnode->title, 'node/' . $trnode->nid),
      );

      $operations = '';

      if(node_access('update', $trnode)) {
        $operations .= l(t('Edit'), 'node/' . $trnid . '/edit', array(), 
                     'destination=node/' . $form['nid']['#value'] . '/translations');
      }
      if((_localizer_access_translation($n) || $node->pid==$trnode->nid) && user_access('Localizer content: disconnect translations')) {
        if($operations) $operations .= ' ';
        $operations .= l(t('Disconnect'), 'admin/content/localizer/content/translation/remove/' . $trnid, array(), 
                     'destination=node/' . $form['nid']['#value'] . '/translations');
      }
      if($operations) {
        $form['operations'][$l] = array(
          '#type' => 'item',
          '#value' =>  $operations ,
        );
      }
    }
    else {
      if($node->language == $l) continue;
      if(_localizer_access_translation($n)) {
        if(user_access('Localizer content: view all available translations')) {
          $r = db_query("SELECT n.nid, n.title FROM {node} n LEFT JOIN {localizernode} l ON n.nid=l.nid WHERE (SELECT COUNT(l2.nid) FROM {localizernode} l2 WHERE l2.pid=l.pid AND l2.language='%s')=0 AND l.language='%s'", $node->language, $l);
        }
        else if(user_access('Localizer content: view own available translations')) {
          $r = db_query("SELECT n.nid, n.title FROM {node} n LEFT JOIN {localizernode} l ON n.nid=l.nid WHERE (SELECT COUNT(l2.nid) FROM {localizernode} l2 WHERE l2.pid=l.pid AND l2.language='%s')=0 AND l.language='%s' AND n.uid=%d", $node->language, $l, $user->uid);
        }

        $nodes = array();
        while($item = db_fetch_array($r)) {
          $nodeavail = node_load($item['nid']);
          if(node_access('view', $nodeavail, $user)) {
            $nodes[$item['nid']] = $item['title'];
          }
        }

        $nodes = array(-1=>t('- Select -')) + $nodes;
        $form['languages']['nodefor|' . $l] = array(
          '#type' => 'select',
          '#title' => t($n),
          '#default_value' => $node->pid,
          '#options' => $nodes
        );
      }
      else {
        $form['languages']['nodefor|' . $l] = array(
          '#title' => t($n),
          '#value' => t('Not assigned'),
        );
      }
      if(_localizer_access_translation($n) && user_access('Localizer content: create translations')) {
        $form['operations'][$l] = array(
          '#type' => 'item',
          '#value' => l(t('Create'), 'admin/content/localizer/content/translation/create/' . $form['nid']['#value'] . '/' . $l),
        );
      }
    }
  }
  if(_localizer_access_translation($languages[$node->language]) && user_access('Localizer content: disconnect translations')) {
    $form['remove'] = array(
      '#type' => 'item',
      '#value' => l(t('Disconnect'), 'admin/content/localizer/content/translation/remove/' . $node->nid, array(), 'destination=node/' . $node->nid . '/translations'),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function theme_localizernode_translations($form) {
  $o = '';
  $header = array(
                  t('Language'),
                  t('Translation'),
                  t('Operations'),
  );
  $rows = array();
  foreach(element_children($form['languages']) as $key) {
    $title = $form['languages'][$key]['#title'];
    unset($form['languages'][$key]['#title']);
    $lang = substr($key,8);
    $rows[] = array(
                    $title,
                    drupal_render($form['languages'][$key]),
                    drupal_render($form['operations'][$lang]),
                   );
  }
  $o .= '<div class="container-inline">' . drupal_render($form['node_language']) . '</div>';
  $o .= drupal_render($form['remove']);
  $o .= theme('table', $header, $rows);
  $o .= drupal_render($form);
  return $o;
}

function localizernode_translations_submit($form_id, $form_values) {
  $op = $form_values['op'];
  $nid = $form_values['nid'];
  $pid = $form_values['pid'];
  $language = $form_values['language'];
  $supported_languages = localizer_supported_languages();
  if($op == t('Save')) {
    foreach($form_values as $key=>$value) {
      if(substr($key, 0, 7)=='nodefor' && $value>0) {
        db_query("UPDATE {localizernode} SET pid=%d WHERE nid=%d", $pid, $value);
        drupal_set_message(t($supported_languages[$lang] . ' translation successfully added to this set.'));
      }
    }
  }
}

function localizernode_translation_remove($nid) {
  if($nid) {
    $supported_languages = localizer_supported_languages();
    $node = node_load($nid);
    $pid = $node->pid;
    db_query('UPDATE {localizernode} SET pid=%d WHERE nid=%d', $nid, $nid);
    $r = db_query('SELECT nid FROM {localizernode} WHERE pid=%d AND nid<>%d', $pid, $nid);
    $nids = array();
    while($i = db_fetch_array($r)) {
      $nids[] = $i['nid'];
    }

    $newpid = 0;
    foreach($nids as $k=>$nid) {
      if(!$newpid) $newpid = $nid;
      db_query('UPDATE {localizernode} SET pid=%d WHERE nid=%d', $newpid, $nid);
    }
    drupal_set_message(t($supported_languages[$node->language] . ' translation successfully removed from this set.'));
    drupal_goto();
  }
}

function localizernode_translation_create($nid, $language) {
  global $user;

  $node = node_load($nid);
  $pid = $node->pid;
  $node->uid = $user->uid;
  $node->language = $language;
  $node->pid = $pid;
  $node->nid = NULL;
  $node->created = 0;
  $node->menu = NULL;
  $node->path = NULL;
  node_save($node);
  drupal_goto('node/'. $node->nid . '/edit');
}

function localizernode_db_rewrite_sql($query, $primary_table, $primary_field) {
  global $user;
  $sql = array();

  $applylocalizer = ($primary_table == 'n');
  $applylocalizer = $applylocalizer && !preg_match('/' . $primary_table . '\.nid\s*=\s*\'?(%d|\d+)\'?/', $query);

  if(module_exists('gsitemap') && !variable_get('localizer_switch_byhostname', FALSE)) {
    if(($primary_table == 'n') && preg_match('/gsitemap/', $query)) {
      //Don't apply localizer because we are serving a 
      //gsitemap request on a site with switchbyhostname
      $applylocalizer = FALSE;
    }
  }

  if(module_exists('views') && variable_get('localizer_global_viewsupport', TRUE)) {
      $applylocalizer = $applylocalizer || ($primary_table == 'node');
  }

  if($applylocalizer) {
    $contents_types_enabled = variable_get('localizer_contents_types', array());
    preg_match('/type\s*=\s*\'\S*\'/', $query, $matches);
    $pos = strpos($matches[0], "'");
    $type = str_replace("'", "", substr($matches[0], $pos));
    if($type && !$contents_types_enabled[$type]) $applylocalizer = FALSE;
  }

  if ($applylocalizer && arg(0) == 'search' && variable_get('localizer_search_all_languages', FALSE)) {
      $applylocalizer = FALSE;
  }

  if ($applylocalizer) {
    $is_book = false;
    if(arg(0)=='node' && arg(1)=='add' && arg(2)=='book') {
      $is_book = true;
      $language = localizer_get_language();
    }
    else if(arg(0)=='node' && arg(1) && is_numeric(arg(1))) {
      $node = node_load(arg(1));
      if($node->type=='book') {
        $is_book = true;
        $language = $node->language;
      }
    }
    if($is_book && $language) {
      $sql['join'] = "LEFT JOIN {localizernode} loc ON loc.nid=" . $primary_table . ".nid";
      $sql['where'] = "loc.language='" . $language . "' OR loc.language IS NULL";
    }
    else if(variable_get('localizer_fallback_support', TRUE)) {
      $languages = localizer_supported_languages();
      $w = '';
      foreach($languages as $l=>$n) {
        if($l==localizer_get_language()) {
          $weight = -100;
        }
        else {
          $fallbackorder = 'fallbacklangorder_' . $l;
          if($user->uid && ($user->$fallbackorder)) {
            $weight = $user->$fallbackorder;
          }
          else {
            $weight = variable_get("localizer_language_fallback_weight_" . $l,0);
          }
        }

        if($w) {
          $w .= ' UNION ';
        }
        $w .= "SELECT $weight AS weight, CAST('$l' AS CHAR(10)) AS language";
      }

      $sql['join'] = "LEFT JOIN {localizernode} loc ON loc.nid=" . $primary_table . ".nid LEFT JOIN($w) lanw ON lanw.language=loc.language LEFT JOIN (select pid, MIN(weight) AS minweight from {localizernode} loc2 LEFT JOIN ($w) lanw2 on lanw2.language=loc2.language group by pid) loc1 ON loc.pid=loc1.pid ";
      $sql['where'] = 'lanw.weight=loc1.minweight';
    }
    else {
      $language = localizer_get_language();
      $sql['join'] = "LEFT JOIN {localizernode} loc ON loc.nid=" . $primary_table . ".nid";
      $sql['where'] = "loc.language='" . $language . "' OR loc.language IS NULL";
    }
  }
  return $sql;
}

function localizernode_content() {
  $o = drupal_get_form('localizernode_content_find');
  $o .= drupal_get_form('localizernode_content_list');

  print theme('page', $o);
}

function localizernode_content_find() {
  $form = array();

  $form['language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#default_value' => $_SESSION['localizer_content_find']['language'] ? $_SESSION['localizer_content_find']['language'] : localizer_get_language(),
    '#options' => localizer_supported_languages(),
  );

  $form['language_translations'] = array(
    '#type' => 'select',
    '#size' => 5,
    '#multiple' => true,
    '#title' => t('Language translations'),
    '#description' => t('No selection means all.<br />Select/deselect criteria by clicking on the item while holding ctrl-key.'),
    '#default_value' => $_SESSION['localizer_content_find']['language_translations'],
    '#options' => localizer_supported_languages(),
  );

  $form['translations_exist'] = array(
    '#type' => 'select',
    '#size' => 5,
    '#multiple' => true,
    '#title' => t('With language translations'),
    '#description' => t('No selection means not apply.<br />Select/deselect criteria by clicking on the item while holding ctrl-key.'),
    '#default_value' => $_SESSION['localizer_content_find']['translations_exist'],
    '#options' => localizer_supported_languages(),
  );

  $form['translations_not_exist'] = array(
    '#type' => 'select',
    '#size' => 5,
    '#multiple' => true,
    '#title' => t('Without language translations'),
    '#description' => t('No selection means not apply.<br />Select/deselect criteria by clicking on the item while holding ctrl-key.'),
    '#default_value' => $_SESSION['localizer_content_find']['translations_not_exist'],
    '#options' => localizer_supported_languages(),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $_SESSION['localizer_content_find']['title'],
  );

  if(!$_SESSION['localizer_content_find']['results_per_page']) {
    $_SESSION['localizer_content_find']['results_per_page'] = 5;
  }
  $form['results_per_page'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Results per page'),
    '#default_value' => $_SESSION['localizer_content_find']['results_per_page'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search')
  );

  return $form;
}

function localizernode_content_find_submit($form_id, $form_values) {
  if ($form_values['op'] == t('Search')) {
    $_SESSION['localizer_content_find']['language'] = $form_values['language'];
    $_SESSION['localizer_content_find']['language_translations'] = $form_values['language_translations'];
    $_SESSION['localizer_content_find']['translations_exist'] = $form_values['translations_exist'];
    $_SESSION['localizer_content_find']['translations_not_exist'] = $form_values['translations_not_exist'];
    $_SESSION['localizer_content_find']['results_per_page'] = $form_values['results_per_page'];
    return 'admin/content/localizer/content';
  }
};

function theme_localizernode_content_find($form) {
  drupal_add_js('misc/collapse.js', 'core', 'header');
  $o  = '';
  $o .= '<fieldset class="collapsible collapsed"><legend>' . t('Search') . '</legend>';
  $o .= '<table><tbody style="border: none;">';
  $o .= '<tr>';
  $o .= '<td>' . drupal_render($form['language']) . '</td>';
  $o .= '<td>' . drupal_render($form['language_translations']) . '</td>';
  $o .= '</tr>';
  $o .= '<tr>';
  $o .= '<td>' . drupal_render($form['translations_exist']) . '</td>';
  $o .= '<td>' . drupal_render($form['translations_not_exist']) . '</td>';
  $o .= '</tr>';
  $o .= '<tr>';
  $o .= '<td colpsan="2">' . drupal_render($form['title']) . '</td>';
  $o .= '</tr>';
  $o .= '<tr>';
  $o .= '<td>' . drupal_render($form['submit']) . '</td>';
  $o .= '<td>' . drupal_render($form['results_per_page']) . '</td>';
  $o .= '</tr>';
  $o .= '</tbody></table>';
  $o .= drupal_render($form);
  $o .= '</fieldset';
  return $o;
}

function localizernode_content_list() {
  global $user;
  $form = array();

  $languages = localizer_supported_languages();
  $language_translations = $_SESSION['localizer_content_find']['language_translations'];

  $header = array(
    array (
      'data' => t('Language'),
      'field' => 'l.language'),
    array (
      'data' => t('Title'),
      'field' => 'n.title'),
    array (
      'data' => t('Operations')),
  );

  $cond = '';
  $language = $_SESSION['localizer_content_find']['language'] ? $_SESSION['localizer_content_find']['language'] : localizer_get_language();
  if($language) {
    if($cond) $cond .= ' AND ';
    $cond .= "language='" . $language ."'";
  }

  $translations_exist = $_SESSION['localizer_content_find']['translations_exist'];
  if($translations_exist && is_array($translations_exist)) {
    if($cond) $cond .= ' AND ';
    $l_string = implode($translations_exist, "','");
    $count = sizeof($translations_exist);
    $cond .= "(SELECT COUNT(l1.nid) FROM {localizernode} l1 WHERE l1.pid=l.pid AND l1.language IN('" . $l_string . "')) = " . $count;
  }

  $translations_not_exist = $_SESSION['localizer_content_find']['translations_not_exist'];
  if($translations_not_exist && is_array($translations_not_exist)) {
    if($cond) $cond .= ' AND ';
    $l_string = implode($translations_not_exist, "','");
    $cond .= "(SELECT COUNT(l2.nid) FROM {localizernode} l2 WHERE l2.pid=l.pid AND l2.language IN('" . $l_string . "')) = 0";
  }

  if(user_access('Localizer content: view all available translations')) {
  }
  else if(user_access('Localizer content: view own available translations')) {
    if($cond) $cond .= ' AND ';
    $cond .= " n.uid=" . $user->uid;
  }

  $s = "SELECT n.nid, n.title, l.language, l.pid FROM {node} n LEFT JOIN {localizernode} l ON n.nid=l.nid";
  if($cond) $s .= ' WHERE ' . $cond;

  $s .= tablesort_sql($header);

  $r = pager_query($s, $_SESSION['localizer_content_find']['results_per_page'], 0, NULL);
  $rows = array();
  while($item = db_fetch_array($r)) {
    $node = node_load($item['nid']);
    if(!node_access('view', $node, $user)) continue;
    $translations = localizernode_get_translations($item['nid']);

    foreach($languages as $l=>$n) {
      if($l == $item['language']) continue;
      if(sizeof($language_translations)>0 && !array_key_exists($l, $language_translations)) continue;

      $key = 'nodefor-' . $item['nid'] . '-' . $item['pid'] . '-' . $l;
      $form['nodes'][$item['nid']]['translations']['language'][$key] = array (
        '#value' => $n,
      );

      if(array_key_exists($l, $translations)) {
        $trnode = $translations[$l];
        $form['nodes'][$item['nid']]['translations']['title'][$key] = array (
          '#value' => l($trnode->title, 'node/' . $trnode->nid),
        );

        $operations = '';
        if(node_access('update', $trnode)) {
          $operations .= l(t('Edit'), 'node/' . $trnode->nid . '/edit', array(), 'destination=admin/content/localizer/content');
        }
        if((_localizer_access_translation($n) || $node->pid==$trnode->nid) && user_access('Localizer content: disconnect translations')) {
          if($operations) $operations .= ' ';
          $operations .= l(t('Disconnect'), 'admin/content/localizer/content/translation/remove/' . $trnode->nid, array(), 'destination=admin/content/localizer/content');
        }
        if($operations) {
          $form['nodes'][$item['nid']]['translations']['operations'][$key] = array (
            '#value' =>  $operations,
          );
        }
      }
      else {
        if(_localizer_access_translation($n)) {
        if(user_access('Localizer content: view all available translations')) {
          $r_nodes = db_query("SELECT n.nid, n.title FROM {node} n LEFT JOIN {localizernode} l ON n.nid=l.nid WHERE (SELECT COUNT(l2.nid) FROM {localizernode} l2 WHERE l2.pid=l.pid AND l2.language='%s')=0 AND l.language='%s'", $item['language'], $l);
        }
        else if(user_access('Localizer content: view own available translations')) {
          $r_nodes = db_query("SELECT n.nid, n.title FROM {node} n LEFT JOIN {localizernode} l ON n.nid=l.nid WHERE (SELECT COUNT(l2.nid) FROM {localizernode} l2 WHERE l2.pid=l.pid AND l2.language='%s')=0 AND l.language='%s' AND n.uid=%d", $item['language'], $l, $user->uid);
        }

          $nodes = array();
          while($tritem = db_fetch_array($r_nodes)) {
            $nodes[$tritem['nid']] = $tritem['title'];
          }
          $form['nodes'][$item['nid']]['translations']['title'][$key] = array (
            '#type' => 'select',
            '#options' => array(-1=>t('- Select -')) + $nodes,
          );
        }
        else {
          $form['nodes'][$item['nid']]['translations']['title'][$key] = array (
          '#title' => t($n),
          '#value' => t('Not assigned'),
          );
        }
        if(_localizer_access_translation($n) && user_access('Localizer content: create translations')) {
          $form['nodes'][$item['nid']]['translations']['operations'][$key] = array (
            '#value' => l(t('Create'), 'admin/content/localizer/content/translation/create/' . $item['nid'] . '/' . $l),
          );
        }
      }
    }

    $form['nodes'][$item['nid']]['language'] = array (
      '#type' => 'checkbox',
      '#title' => $languages[$item['language']]
    );

    $form['nodes'][$item['nid']]['title'] = array (
      '#value' => l($item['title'], 'node/' . $item['nid'])
    );

    $operations = '';
    if(node_access('update', $node)) {
      $operations .= l(t('Edit'), 'node/' . $node->nid . '/edit', array(), 'destination=admin/content/localizer/content');
    }
    if(_localizer_access_translation($languages[$node->language]) && user_access('Localizer content: disconnect translations')) {
      if($operations) $operations .= ' ';
        $operations .= l(t('Disconnect'), 'admin/content/localizer/content/translation/remove/' . $node->nid, array(), 'destination=admin/content/localizer/content');
    }
    if($operations) {
      $form['nodes'][$item['nid']]['operations'] = array (
        '#value' =>  $operations,
        );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  $form['pager'] = array('#value' => theme('pager', NULL, $_SESSION['localizer_content_find']['results_per_page'], 0));
  $form['header'] = array('#value' => $header);
  return $form;
}

function localizernode_content_list_submit($form_id, $form_values) {
  if ($form_values['op'] == t('Save')) { 
    $supported_languages = localizer_supported_languages();
    foreach($form_values as $key=>$value) {
      if(substr($key, 0, 7)=='nodefor' && $value>0) {
        $nodefor = explode('-', $key);
        $pid = $nodefor[2];
        db_query("UPDATE {localizernode} SET pid=%d WHERE nid=%d", $pid, $value);
        drupal_set_message(t($supported_languages[$lang] . ' translation successfully added to this set.'));
      }
    }
  }
}

function localizernode_content_list_validate($form_id, $form_values) {
  if ($form_values['op'] == t('Save')) {
    $nodeforlist = array();
    foreach($form_values as $key=>$value) {
      if(substr($key, 0, 7)=='nodefor') {
        $nodeforlist[$key] = $value;
      }
    }

    $nodeforlist = array_reverse($nodeforlist);
    $c = sizeof($nodeforlist);
    for($i=0; $i<$c; $i++) {
      $nodefor = array_slice($nodeforlist,0,1);
      array_shift($nodeforlist);
      $key = key($nodefor);
      $value = $nodefor[$key];
      if($value>0 && in_array($value, $nodeforlist)) {
        form_set_error($key, t('You cannot assign the same translation to multiple sets.'));
      }
    }
  }
}

function theme_localizernode_content_list($form) {
  drupal_add_js('misc/collapse.js', 'core', 'header');
  $pre = '<fieldset class="collapsible collapsed"><legend>' . t('Translations') . '</legend>';
  $suf = '</fieldset>';

  $header = $form['header']['#value'];
  unset($form['header']);

  $rows = array();
  foreach(element_children($form['nodes']) as $nid) {
    $rows[] = array (
      drupal_render($form['nodes'][$nid]['language']),
      drupal_render($form['nodes'][$nid]['title']),
      drupal_render($form['nodes'][$nid]['operations']),
    );

    $trows = array();
    foreach(element_children($form['nodes'][$nid]['translations']['title']) as $key) {
      $trows[] = array (
        drupal_render($form['nodes'][$nid]['translations']['language'][$key]),
        drupal_render($form['nodes'][$nid]['translations']['title'][$key]),
        drupal_render($form['nodes'][$nid]['translations']['operations'][$key]),
      );
    }

    $rows[] = array (
      array (
             'data' => $pre . theme('table', array(), $trows) . $suf,
             'colspan' => 3,
      ),
    );
  }

  $o = theme('table', $header, $rows);
  $o .= drupal_render($form['pager']);
  $o .= drupal_render($form);
  return $o;
}

function _localizernode_book_parents_js($language=NULL, $nid_exclude=0) {
  if(!$language) $language = localizer_get_language();
  $result = db_query("SELECT n.nid, n.title, b.parent, b.weight FROM {node} n INNER JOIN {book} b ON n.vid = b.vid INNER JOIN {localizernode} l ON n.nid=l.nid WHERE n.status = 1 AND l.language='%s' ORDER BY b.weight, n.title", $language);

  $children = array();
  while ($node = db_fetch_object($result)) {
    if (!$children[$node->parent]) {
      $children[$node->parent] = array();
    }
    $children[$node->parent][] = $node;
  }

  $toc = array();
  // If the user has permission to create new books, add the top-level book page to the menu;
  if (user_access('create new books')) {
    $toc[0] = '<'. t('top-level') .'>';
  }

  $toc = book_toc_recurse(0, '', $toc, $children, $nid_exclude);

  print drupal_to_js($toc);
  exit();
}

?>
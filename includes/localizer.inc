<?php
if(module_exists('localizernode')) {
include_once(drupal_get_path('module', 'localizer') . '/includes/localizernode.inc');
}


function localizer_supported_languages() {
  global $user;
  static $languages;
  if(!isset($languages)) {
    if (function_exists('locale')) {
      $languages = locale_supported_languages();
      $languages = $languages['name'];
    }
    else {
      $languages = array('en' => 'English');
    }
  }

  if($user->uid && $user->languages) {
    $user->languages = localizer_clear_languagesarray($user->languages);
    $languages = array_intersect_key($languages, $user->languages);
  }
  return $languages;
}

function localizer_supported_languages_all() {
  static $languages;
  if(!isset($languages)) {
    if (function_exists('locale')) {
      $languages = locale_supported_languages();
      $languages = $languages['name'];
    }
    else {
      $languages = array('en' => 'English');
    }
  }
  return $languages;
}

function localizer_get_default_language() {
  $languages = localizer_supported_languages();
  return key($languages);
}

function localizer_isvalid_language($language = '') {
  static $valid_languages;
  $valid_languages = localizer_supported_languages();

  if(($language != '') && array_key_exists($language, $valid_languages)){
    return true;
  }
  else
  {
    return false;
  }
}

function localizer_get_language() {
  global $locale;
  if(!$locale) {
    $locale = localizer_get_default_language();
  }
  return $locale;
}

function localizer_set_language($language = '') {
  static $valid_languages;
  $valid_languages = localizer_supported_languages();

  if(($language != '') && ($language != localizer_get_language()) && (array_key_exists($language, $valid_languages))) {
    global $locale;
    $locale=$language;
    $_SESSION['current_locale']=$locale;
  }
}

function localizer_get_localized_path($path, $language) {
  $s = drupal_lookup_path('source', $path);
  if($s) $path = $s;
  if(function_exists('localizernode_get_localized_path')) {
    $lpath = localizernode_get_localized_path($path, $language);
  }
  else {
    $lpath = $path;
  }
  $a = drupal_lookup_path('alias', $lpath);
  if($a) $lpath = $a;
  return $lpath;
}

function localizer_exists_localized_content($path, $language) {
  if(function_exists('localizernode_exists_localized_content')) {
    return localizernode_exists_localized_content($path, $language);
  }
  else {
    return false;
  }
}

function localizer_get_content_language($_path) {
  if(function_exists('localizernode_get_content_language')) {
    return localizernode_get_content_language($_path);
  }
  return localizer_get_default_language();
}

function to($object_name, $object_key, $object, $language=NULL) {
  if(!$language) $language = localizer_get_language();

  $items = localizertranslation_findall("object_name='$object_name' AND object_key='$object_key' AND language='" . $language . "'");

  foreach($items as $key=>$item) {
    if(!empty($item['translation'])) {
      $object_field=$item['object_field'];
      $object->$object_field=$item['translation'];
    }
  }
  return $object;
}

function ts($string, $language=NULL) {
  if(!$language) $language = localizer_get_default_language();

  $t = localizertranslation_findone("object_name='string' AND object_key='$string' AND object_field='value' AND language='" . $language . "'");
  if(array_key_exists('tid', $t) && $t['tid']) {
    $tstring = $t['translation'];
  }
  else {
    $tstring = $string;
    $t = array (
                   'object_name' => 'string',
                   'object_key' => $string,
                   'object_field' => 'value',
                   'language' => $language,
                   'translation' => $string,
                  );
    localizertranslation_insert($t);
  }
  return $tstring;
}

function localizer_path_without_language($path) {
  $exploded_path = explode('/', $path);
  $languageinpath = $exploded_path[0];
  if(localizer_isvalid_language($languageinpath)){
    array_shift($exploded_path);
    return implode("/", $exploded_path);
  }
  else
  {
    return $path;
  }
}

function localizer_clear_languagesarray($options, $onlystrings=false) {
  if(is_array($options)) {
    foreach($options as $key=>$value) {
      if($value=='0') unset($options[$key]);
      if($onlystrings && is_integer($value)) unset($options[$key]);
    }
  }
  return $options;
}

?>